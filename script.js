function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox'], headless: false});
  const page = await browser.newPage();
  await page.goto('https://www.linkedin.com');
  await page.screenshot({path: 'example.png'});

  await sleep(2000);

  await page.click(".login-email");

  let email = "foobar2718281828@gmail.com"

  for (var i = 0; i < email.length; i++) {
    await page.keyboard.type(email.charAt(i) + "");
    await sleep(100)
  }

  await sleep(1000);  

  await page.keyboard.press("Tab");

  await sleep(1000);

  let password = "2.718281828"

  for (var j = 0; j < password.length; j++) {
    await page.keyboard.type(password.charAt(j) + "");
    await sleep(100)
  }

  await sleep(1000);

  await page.keyboard.press("Enter")

  for (var counter = 0; counter < 20; counter++) {
    await sleep(1000);
    console.log(counter)
  }

  //await page.goto('https://www.linkedin.com/feed/', {"waitUntil" : "networkidle0"});

  const search_bar = await page.$x("//input[@placeholder='Search']");

  await search_bar[0].click()

  await sleep(2000);

  let name = "Yev Barkalov"

  for (var k = 0; k < name.length; k++) {
    await page.keyboard.type(name.charAt(k) + "")
    await sleep(100);
  }
  
  await sleep(500);

  await page.keyboard.press("ArrowDown");

  await sleep(250);

  await page.keyboard.press("Enter");

  await sleep(5000);

  const search_bar2 = await page.$x("//input[@placeholder='Search']");

    await search_bar2[0].click()

      await sleep(250)
  
  await page.keyboard.press("ArrowDown")

  await sleep(2000);

  let name2 = "Zach Latta"

  for (var i2 = 0; i2 < name2.length; i2++) {
    await page.keyboard.type(name2.charAt(i2) + "")
    await sleep(100)
  }

  await sleep(500)
  
  await page.keyboard.press("ArrowDown")

  await sleep(250)
  
  await page.keyboard.press("ArrowDown")

  await sleep(250)
    await page.keyboard.press("Enter");

      await sleep(250)
  
  await page.keyboard.press("ArrowDown")

  await sleep(250)
  
  await page.keyboard.press("ArrowDown")

    await sleep(5000);

    const search_bar3 = await page.$x("//input[@placeholder='Search']");

  await search_bar3[0].click()

  await sleep(2000);

    let name3 = "Senec"

  for (i2 = 0; i2 < name3.length; i2++) {
    await page.keyboard.type(name3.charAt(i2) + "")
    await sleep(100)
  }

  await sleep(500)
  
  await page.keyboard.press("ArrowDown")

  await sleep(250)
  await page.keyboard.press("Enter");

  await sleep(5000);

  await browser.close()
})();
